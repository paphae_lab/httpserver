package top.paphae.httpServer.handler;


import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import top.paphae.httpServer.util.HttpUtil;

/**
 * 处理/add路径请求的处理器类
 */
public class AddHttpHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) {
        try {
            StringBuilder responseText = new StringBuilder();
            String requestParam = HttpUtil.getRequestParam(httpExchange);
            String[] split = requestParam.split("&");
            int a = Integer.parseInt(split[0].replace("a=",""));
            int b = Integer.parseInt(split[1].replace("b=",""));
            responseText.append(a + b);
            HttpUtil.handleResponse(httpExchange, responseText.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

