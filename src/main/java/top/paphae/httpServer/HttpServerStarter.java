package top.paphae.httpServer;

import com.sun.net.httpserver.HttpServer;
import top.paphae.httpServer.handler.AddHttpHandler;
import top.paphae.httpServer.handler.MultHttpHandler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

public class HttpServerStarter {
    public static void main(String[] args) throws IOException {
        HttpServer httpServer = HttpServer.create(new InetSocketAddress(8080), 0);

        httpServer.createContext("/add", new AddHttpHandler());
        httpServer.createContext("/mult", new MultHttpHandler());

        httpServer.setExecutor(Executors.newFixedThreadPool(10));

        httpServer.start();
    }
}