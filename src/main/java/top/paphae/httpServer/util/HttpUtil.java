package top.paphae.httpServer.util;

import com.sun.net.httpserver.HttpExchange;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class HttpUtil {
    /**
     * 获取请求参数
     * @param httpExchange
     * @return paramStr
     * @throws Exception
     */
    public static String getRequestParam(HttpExchange httpExchange) {
        String paramStr = "";

        if ("GET".equals(httpExchange.getRequestMethod())) {
            paramStr = httpExchange.getRequestURI().getQuery();
        }

        return paramStr;
    }

    /**
     * 处理响应
     * @param httpExchange 此类封装了收到的HTTP请求和在一个交换中生成的响应
     * @param responseText 相应文本内容
     * @throws Exception
     */
    public static void handleResponse(HttpExchange httpExchange, String responseText) throws Exception {
        StringBuilder responseContent = new StringBuilder();
        responseContent.append("<html>")
                .append("<body>")
                .append(responseText)
                .append("</body>")
                .append("</html>");
        String responseContentStr = responseContent.toString();
        byte[] responseContentByte = responseContentStr.getBytes(StandardCharsets.UTF_8);

        //设置响应头，必须在sendResponseHeaders方法之前设置！
        httpExchange.getResponseHeaders().add("Content-Type:", "text/html;charset=utf-8");

        //设置响应码和响应体长度，必须在getResponseBody方法之前调用！
        httpExchange.sendResponseHeaders(200, responseContentByte.length);

        OutputStream out = httpExchange.getResponseBody();
        out.write(responseContentByte);
        out.flush();
        out.close();
    }
}
